var map = L.map('main_map').setView([-34.437048, -58.788649], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/coypright">OpenStreetMap</a> contributors'
}).addTo(map);


L.marker([20.5910949, -100.3923593]).addTo(mymap);
L.marker([20.6134982, -100.4050343]).addTo(mymap);
L.marker([20.5843947, -100.3838736]).addTo(mymap);


$.ajax({
    dataType: "json",
    url: "api/bikes",
    success: function(result){
        console.log(result);
        result.bikes.forEach(function(bike){
        L.marker(bike.location,{title: bike.id}).addTo(map);
        });
    }
})